# CSE 109 - Systems Software - Spring 2021

## Instructor

- [Prof. Corey Montella](https://engineering.lehigh.edu/cse/faculty/5300)
- E-mail Address: [cim310@lehigh.edu](mailto:cim310@lehigh.edu)
- Gitlab: https://gitlab.com/cmontella
- Office Hours: By appointment

## Course Description
   
Advanced programming and data structures, including dynamic structures, memory allocation, data organization, hash tables, data files. Object-oriented design and implementation of simple assemblers, loaders, interpreters, compilers and translators. Practical methods for implementing medium-scale programs.
   
## Course Learning Objectives

- Introduction to UNIX and emacs
- C and C++
- Review of structures shared by Java and C++
- Passing variable to functions
- Pointers
- Makefiles
- Hashing, Data Structures
- Text file reading and writing
- Binary file reading and writing
- Classes and Subclasses
- Templated Classes
- Debugging using gdb
- Assembler Basics
- Linker Basics
- Loader Basics
- Lexical Analysis
- Parsing
- Virtual Machines
- Networking
- Building Mid-Size Programs

## Prerequisites

CSE017 or CSE018

I assume you know at least the Java programming language, as well as basic programming competency and knowledge of data structures and algorithms. Basic programming competency in this case means the ability to use variables, to write and call functions, to use for/while loops, and to use recursion to solve problems. If you can write a recursive linked list implementation, you are ready to take this course. If anything in that sentence is unclear to you, you should brush up on your programming abilities before taking this course.

## Online Classroom

We will use a number of online tools to help administer this course in an online-first way. All lectures and recitations will be recorded and available on the course Google Drive at the link in the list below. We will conduct most of our communication via Slack, which will help keep us connected as a class. 

Each week we will have two classes and a recitation on Friday. Throughout the week, you will have the opportunity to submit questions for me to answer during th recitation. These questions will count toward your participation grade. You can also ask questions live during recitation.

- [Repository](http://gitlab.cse.lehigh.edu/cse109/spring-2022) - this will serve as our course website for the semester. All course resources will be centralized here.
- [Assignments](http://gitlab.cse.lehigh.edu/cse109/spring-2022/assignments) - this private repository is where assignments and exams will be posted.
- [Zoom Class Link](https://lehigh.zoom.us/j/5245765302) - Used for recitations and instructor office hours.
- [Lectures](https://drive.google.com/drive/folders/1azSC_vhbAAWa8Cbq3UYIFD5b9WDgmIyU?usp=sharing) - A playlist of lectures, to be updated after each class.
- [Slack](https://join.slack.com/t/lu-cse109-sp22/shared_invite/zt-11ymus4jr-USZ3IvM_7_ZJ0AXyVtURSA) - We will use Slack for synchronous communication. I will post updates on new assignments there, as well as class announcements.
- [Piazza](piazza.com/lehigh/spring2022/cse109/home) - You can ask and answer questions about course material on Piazza. There is a feature for anonymous posting (the instructor can still see who you are). Using Piazza to ask, answer, or even view questions counts toward your participation grade.
- [Coursesite](https://coursite.lehigh.edu) - Coursesite will be used exclusively to distribute grades in a secure manner. **There will be no course content posted on Coursesite.**

## Required Texts

Think C/C++

by Allen B. Downey

Along with a variety of online resources, we will use this textbook as a reference for the C and C++ languages:

https://greenteapress.com/wp/think-c/

## Evaluation Criteria

- Participation - 10%
- Homework - 30%
- Quizzes - 20%
- Midterm - 20%
- Final - 20% 

### Participation

Participation points are earned by engaging with the class through Piazza or during recitation. Activities that count include asking questions, answering questions, sharing links, and even viewing questions that have been asked by other students. You are free to ask questions anonymously to the class, but the instructor will always know the identity of a poster on Piazza.

There are 100 possible participation points.

- Answering a question on Piazza is worth 10 points.
- Asking a question on Piazza or submitting a question to be answered during recitation is worth 10 points. (Maximum 30 points for multiple)
- Submitting relevant links and viewing Piazza posts are worth 1 point each. (Maximum 60 points all together)

Attending recitation on Friday is optional and will not count toward your participation grade.

### Homework

Homework will be assigned on a nominally weekly basis. It may take the form of programming assignments, written assignments, or solving problem sets. Some assignments will be partially automatically graded against a set of tests. All grades will be assigned by a team of graders. 

Appeals for grades must be made in writing to the instructor within 1 week after graded assignments are returned.

### Quizzes

Quizzes will be assigned at the end of each week to be completed by the end of the day on Friday. Quizzes are pass/fail and will test your understanding of the week's lecture topics. They will be completely open book, and open internet. Late quizzes are not accepted, no exceptions.

### Exams

We will have midterm and one comprehensive final exam this semester. They will both me take-home exam, and will be open book and open internet. Late exams are not accepted, no exceptions.

## Course Policies

### Covid19 Statement

Due to the ongoing pandemic, this course will be held partially online. If you are attending online, you can choose whether or not to turn on your camera. If you choose to turn on your camera, please make sure to present yourself as you would in a live classroom setting. As the pandemic is constantly evolving, we reserve the right to make updates to this syllabus in response. This includes the option to take the class entirely online as needed.

### Time Zones

Unless otherwise specified, all deadlines and meeting times are communicated relative to Lehigh University, which sits in the Eastern time zone. At the beginning of the course the time shift is GMT-4, but on Sunday, November 1 our clocks will be adjusted back one hour for the remaining of the semester. This may or may not coincide with a similar shift in time for your local region, so please be cognizant of this possibility when submitting assignments.

### Version Control

We will use a version control system (VCS) called "git" to turn in all assignments and exams in this course. This website, Gitlab, is an online interface for the git VCS, but it is also available as a command line utility and a desktop utility. You can do the work in this course in any of these contexts.

Git is useful for a couple reasons. First, it is a practical tool that is used commonly in the software development industry. It is useful for coordinating the work of a team of developers that may be distributed across the globe. This means you choose a career as a software developer, you will likely use a tool like git as part of your job, so it's great practice to get used to using it here. But more importantly, in the context of a classroom, git allows the instructor to monitor the progress of your work to detect anomalies like cheating. 

### Late Assignments

Late assignments will incur a penalty of one letter grade (10%) per day late, up to two late days. After two days late the assignment will not be accepted (as the solution will have been posted by then). The lowest homework assignment grade will be dropped when calculating your final grade.

### Exam Makeup

All students are expected to complete the final exam during the scheduled time. If you have an excused conflict, we can accommodate your schedule. Excused conflicts are those activities sanctioned by the University, or any medical condition that necessitates an exception is warranted. Please make arrangements with the instructor for an alternative testing situation. Inform the instructor as soon as you know a conflict exists.

### Office Hours

In general, I will only hold office hours by appointment. If you want one-on-one help, I will be more than happy to meet with you over Zoom, or you can join a zoom chat with one of our many graders. However, **no in-person office hours will be granted by the instructor within 24 hours of an assignment or exam deadline**. This is to encourage you to start your assignments and studying early.

### Extra Credit

Extra credit up to 5% of the final grade is available for this course. Credit can be earned by submitting a patch to the [Mech programming language](https://gitlab.com/mech-lang). If you plan on doing extra credit work, please speak with the instructor first for instructions on how to submit your work. The amount of extra credit to be awarded will be at the instructor's discretion.

### Statement on Academic Integrity

The programs you write in this course should be original and written for this course specifically. You can't submit solutions found on the internet, and you can't use projects you've written for other courses. At times we will use third party libraries or example code as a basis for your own code. This is permissible if the included code is cited.

It is permissible in this course to debug programs collaboratively. You can ask for help on Piazza or Course Site, but please don't post too much of your code.

My view is that students resort to cheating when they feel desperate. If you're running out of time and you need an extension, ask for one rather than cheat. If you are completely lost and feel like you don't understand the assignment at all, talk to a grader or the instructor and they will help you get started. If you're worried about getting a poor grade on a project or an exam, consider that a poor grade will be forgotten soon after you graduate, while getting caught cheating will long be remembered. Just submit your own work that represents your best attempt, and you'll probably at least some partial credit.

## University Policies

### The Principles of Our Equitable Community

The Principles of Our Equitable Community: Lehigh University endorses The Principles of Our Equitable Community (www.lehigh.edu/diversity). We expect each member of this class to acknowledge and practice these Principles. Respect for each other and for differing viewpoints is a vital component of the learning environment inside and outside the classroom.

### Accommodations for Students with Disabilities

Lehigh University is committed to maintaining an equitable and inclusive community and welcomes students with disabilities into all of the University's educational programs. In order to receive consideration for reasonable accommodations, a student with a disability must contact
Disability Support Services (DSS), provide documentation, and participate in an interactive review process. If the documentation supports a request for reasonable accommodations, DSS will provide students with a Letter of Accommodations. Students who are approved for accommodations at Lehigh should share this letter and discuss their accommodations and learning needs with instructors as early in the semester as possible. For more information or to request services, please contact Disability Support Services in person in Williams Hall, Suite 301, via phone at 610-758-4152, via email at indss@lehigh.edu, or online at [https://studentaffairs.lehigh.edu/disabilities](https://studentaffairs.lehigh.edu/disabilities).

### Lehigh University Policy on Harassment and Non-Discrimination

Lehigh University upholds The Principles of Our Equitable Community and is committed to providing an educational, working, co-curricular, social, and living environment for all students, staff, faculty, trustees, contract workers, and visitors that is free from harassment and
discrimination on the basis of age, color, disability, gender identity or expression, genetic information, marital or familial status, national or ethnic origin, race, religion, sex, sexual orientation, or veteran status. Such harassment or discrimination is unacceptable behavior and will not be tolerated. The University strongly encourages (and, depending upon the circumstances, may require) students, faculty, staff or visitors who experience or witness harassment or discrimination, or have information about harassment or discrimination in University programs or activities, to immediately report such conduct.
